# EdgeGallery应用部署指导【latest】
## 一 环境检查和系统注册

### 01 安装完成后检查环境
安装过程中最后一步eg_check会检查edgegallery环境，注意查看环境检查的结果
![输入图片说明](image/image1.png)

检查pod状态，所有pod状态为Running则正常，检查命令：kubectl get pod -A
![输入图片说明](image/image2.png)
### 02 登录Edgegallery操作界面并修改登录密码
在chrome浏览器打开https://MASTER_IP:30095网页 [MASTER_IP是在var.yaml 配置的IP，如果配置了PORTAL_IP或代理IP，
就用自己配置的IP] \
管理员账户和密码： \
账号：admin  密码：Admin@321 \
登录后修改密码 \
![输入图片说明](image/image3.png)
### 03 注册沙箱环境

集成开发 → 系统管理 → 沙箱管理 → 新增沙箱环境 \
这里需要根据自己的需要选择要注册的系统 \
k8s系统沙箱环境注册： \
![输入图片说明](image/image4.png)

openstack 系统沙箱环境注册： \
![输入图片说明](image/image5.png)

openstack 配置文件格式【文件名称可以命名为config】： 

```
export OS_USERNAME=admin 
export OS_PASSWORD=xxx     # openstack系统登录密码   
export OS_PROJECT_NAME=admin
export OS_AUTH_URL=http://openstack_ip/identity   # openstack访问ip 
export OS_IDENTITY_API_VERSION=3  
export OS_PROJECT_DOMAIN_NAME=default  
export OS_USER_DOMAIN_NAME=default

```
### 04 MECM系统注册

mecm管理平台 → 系统 → mecm注册系统

![输入图片说明](image/image6.png)

### 05 应用仓库系统注册

mecm管理平台 → 系统 → 应用仓库注册系统 \
仓库密码：在install/password-var.yml 中查找HARBOR_ADMIN_PASSWORD值

![输入图片说明](image/image7.png)

### 06 边缘节点注册

mecm管理平台→边缘节点   \
注册k8s系统作为边缘节点   \
【注意坐标OpenStreeMap查询到的是 纬度，经度 需要修改为 经度,纬度 填入】 

![输入图片说明](image/image8.png)

openstack系统注册，除了IP地址填写openstack访问IP，其填写项目同上k8s系统填写方式一样 

上传配置文件: \
![输入图片说明](image/image9.png)
## 二 部署容器服务
### 01 上传镜像

用以下命令包镜像打包为 image_name.tar格式  \
docker save -o  image_name.tar image_name:image_tag \
集成开发 → 系统管理 → 系统镜像管理 → 新建系统镜像 → 容器  \
选择镜像文件上传 \
![输入图片说明](image/image10.png)

### 02 创建项目
集成开发 → 应用孵化 → 新增应用 → 选择场景或新建应用 【选择场景只应用于数据采集、清洗、整理方面】\
使用选择场景的功能需要先上传需要的镜像【按照01上传镜像操作】： 

```
docker pull swr.cn-north-4.myhuaweicloud.com/edgegallery/profile-manager:v1.0  
docker pull swr.cn-north-4.myhuaweicloud.com/edgegallery/fledge:latest    
docker pull swr.cn-north-4.myhuaweicloud.com/edgegallery/fledge-gui:latest   
docker pull swr.cn-north-4.myhuaweicloud.com/edgegallery/kuipe:1.2.1-slim   
docker pull swr.cn-north-4.myhuaweicloud.com/edgegallery/mec-app-visual-demo:latest  
docker pull swr.cn-north-4.myhuaweicloud.com/egpatner/emqx:latest   
docker pull swr.cn-north-4.myhuaweicloud.com/edgegallery/mqtt-tdengine-adapter:latest   
```

![输入图片说明](image/image39.png) 
![输入图片说明](image/image40.png)   
  
集成开发 → 应用孵化 → 新增应用 → 新建应用 \
![输入图片说明](image/image11.png) 

### 03 能力选择
![输入图片说明](image/image12.png)

选择需要的能力 \
![输入图片说明](image/image13.png)

### 04 选择沙箱

如果沙箱不存在需要去注册沙箱环境 \
![输入图片说明](image/image14.png)
![输入图片说明](image/image15.png)

### 05 上传部署文件
![输入图片说明](image/image16.png)

上传文件完成后点击 关闭 完成 \
![输入图片说明](image/image17.png)

### 06 制作镜像

打包预览 → 确认 → 确认 \
![输入图片说明](image/image18.png)

### 07 测试认证

选择社区通用场景 → 开始测试 【没有测试报告点击返回】 \
![输入图片说明](image/image19.png)

### 08 发布应用

点击发布【发布成功会有消息提示】\
![输入图片说明](image/image20.png)

### 09 查看应用

应用仓库查看发布的应用 \
![输入图片说明](image/image21.png)

【应用分发部署过程】 
### 10 应用进行分发和部署

mecm管理平台 → 应用管理  → 应用包管理  → 从应用市场同步 → 分发&部署 → 分发 → 部署 → 在选择的边缘节点服务器上查看应用的pod \
![输入图片说明](image/image22.png)
![输入图片说明](image/image23.png)
![输入图片说明](image/image24.png)
![输入图片说明](image/image25.png)
## 三 部署虚拟机服务

### 01 上传镜像

集成开发 → 系统管理 → 系统镜像管理 → 新建系统镜像 → 虚拟机 → 填写信息 → 更多 → 上传
选择镜像文件上传 \
![输入图片说明](image/image26.png)
![输入图片说明](image/image27.png)

### 02 创建项目

集成开发 → 应用孵化 → 新增应用 \
![输入图片说明](image/image28.png)

### 03 能力选择

![输入图片说明](image/image29.png)

### 04 选择沙箱

如果沙箱不存在需要去注册沙箱环境 \
![输入图片说明](image/image30.png)
![输入图片说明](image/image31.png)
![输入图片说明](image/image32.png)

### 05 生成镜像包
![输入图片说明](image/image33.png)
![输入图片说明](image/image34.png)
![输入图片说明](image/image35.png)
![输入图片说明](image/image36.png)
![输入图片说明](image/image37.png)
![输入图片说明](image/image38.png)