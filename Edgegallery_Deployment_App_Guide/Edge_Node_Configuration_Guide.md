 ## 边缘节点配置

当EdgeGallery边缘和中心不在同一台服务器上安装的时候，需要在边缘节点去操作Harbor登录的步骤

### 1.配置daemon.json


配置/etc/docker/daemon.json，新增如下字段，其中xxx.xxx.xxx.xxx为中心节点的私有或公网IP，边缘节点需要与中心节点机器互通，若无该文件不存在则需要创建
```
{
    "insecure-registries" : ["xxx.xxx.xxx.xxx"]
}
```
### 2.重启docker服务

```
systemctl restart docker.service
```
### 3.docker登录Harbor
xxx.xxx.xxx.xxx为中心节点的私有或公网IP \
$HARBOR_ADMIN_PASSWORD为中心节点安装前配置的Harbor的密码

```
docker login -u admin -p $HARBOR_ADMIN_PASSWORD xxx.xxx.xxx.xxx
kubectl create secret docker-registry harbor --docker-server=https://xxx.xxx.xxx.xxx --docker-username=admin --docker-password=$HARBOR_ADMIN_PASSWORD
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "harbor"}]}'
```
### 4.按照部署测试文档注册边缘节点
操作部署测试APP请参考指导文件[Edgegallery_Deploying_Application_App_Operation_Instructions.md](Edgegallery_Deploying_Application_App_Operation_Instructions.md)。

