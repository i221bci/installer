#
#   Copyright 2021 Huawei Technologies Co., Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

---

- name: Check whether Docker is installed
  command: docker version
  register: dockerInstalled
  ignore_errors: yes

- debug:
    msg: Docker is already installed, no need to be installed again
  when: dockerInstalled is succeeded

- debug:
    msg: Docker is not installed, will be installed
  when: dockerInstalled is failed

- name: Install Docker Online
  import_tasks: docker_install_online.yml
  when: dockerInstalled is failed

  #- name: Setup Docker Daemon For AIO
  #import_tasks: docker_daemon_set_aio.yml
  #when: NODE_MODE == "aio" and K8S_NODE_TYPE == "master" and HarborIP is defined

- name: Set Docker daemon.json on Worker Node
  copy:
    src: "/home/{{ MASTER_IP }}/{{ groups.master[0] }}/etc/docker/daemon.json"
    dest: /etc/docker/
  when: NODE_MODE == "muno" and K8S_NODE_TYPE == "worker"

- name: Restart Docker Service
  service:
    name: docker.service
    state: restarted

- name: Install K8S
  import_tasks: k8s_install_online.yml

- name: Try to Get Helm Version
  shell: helm version | cut -d \" -f2
  register: helmVersion
  ignore_errors: yes

- name: Helm is Installed
  debug:
    msg: "helm {{ HELM_VERSION }} is already installed"
  when: helmVersion.stdout == (HELM_VERSION)

- name: Helm doesn't be Installed
  debug:
    msg: "helm {{ HELM_VERSION }} is not installed yet"
  when: helmVersion.stdout != (HELM_VERSION)

- name: Unarchive Helm Offline Tarball File
  shell: |
    wget -N https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz
    tar -zxvf helm-v3.8.0-linux-amd64.tar.gz
    cp ./linux-amd64/helm /usr/local/bin/
  register: unarchiveHelm
  when: helmVersion.stdout != (HELM_VERSION)

- name: Check Helm Installed
  shell: helm version
  when: helmVersion.stdout != (HELM_VERSION) and unarchiveHelm is succeeded

- name: Setup NFS for Storage Persistent on Master Node
  import_tasks: nfs_setup_online.yml
  when: K8S_NODE_TYPE == "master" and ENABLE_PERSISTENCE == true

- name: Setup NFS Mount on Worker Node
  import_tasks: nfs_mount_worker_setup.yml
  when: K8S_NODE_TYPE == "worker" and ENABLE_PERSISTENCE == true
